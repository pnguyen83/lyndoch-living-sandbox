public class skedAvailabilityHandler {
	public static void onAfterInsert(List<sked__Availability__c> newAvais) {
        sendPendingUnAvailabilityNotificationEmail(newAvais, null);
    }

    public static void onAfterUpdate(List<sked__Availability__c> newAvais, Map<id, sked__Availability__c> map_id_old) {
        sendPendingUnAvailabilityNotificationEmail(newAvais, map_id_old);
    }

    //==========================Private function====================================//
    //TODO: create a function to send notification email to chatter group when a pending unavailabilty is created
    private static void sendPendingUnAvailabilityNotificationEmail(List<sked__Availability__c> newAvais,
                                                                    Map<id, sked__Availability__c> map_id_old) {
        //get list of pending unavailability
        List<sked__Availability__c> pendingUnAvais = getPendingUnavailability(newAvais, map_id_old);
        //generate email notifications
        List<Messaging.singleEmailmessage> emails = generateUnAvaiEmailBody(pendingUnAvais);
        //get recipient's email addresses
        List<String> recipientAddresses = getrecipientEmailAddress();
        //update recipients email addresses to list of notification emails
        updateEmailRecipientAddresses(recipientAddresses, emails);
        //send emails
        sendEmail(emails);
    }

    private static List<sked__Availability__c> getPendingUnavailability(List<sked__Availability__c> newAvais,
                                                                    Map<id, sked__Availability__c> map_id_old) {
        List<sked__Availability__c> pendingUnAvais = new List<sked__Availability__c>();
        Set<String> pendingUnAvaiIds = new Set<String>();
        //TODO: get pending unavailability from new list
        //if mapold is not null, check if the status is change from another status to pending
        for (sked__Availability__c avai : newAvais) {
            if (avai.sked__Status__c == skedConStants.AVAILABILITY_STATUS_PENDING && avai.sked__Is_Available__c == false) {
                if (map_id_old != null) {
                    if (map_id_old.containsKey(avai.id) && map_id_old.get(avai.id).sked__Status__c != avai.sked__Status__c) {
                        pendingUnAvaiIds.add(avai.id);
                    }
                }
                else {
                    pendingUnAvaiIds.add(avai.id);
                }
            }
        }

        if (!pendingUnAvaiIds.isEmpty()) {
            skedObjectQueryBuilder avaiQueryBuilder = new skedObjectQueryBuilder('sked__Availability__c');
            avaiQueryBuilder.addFields(new Set<String>{
                'id', 'name', 'sked__Resource__c', 'sked__Resource__r.Name', 'sked__Type__c', 'sked__Start__c',
                'sked__Finish__c', 'sked__Status__c', 'sked__Timezone__c'
            });
            avaiQueryBuilder.whereClause = ' WHERE Id IN :pendingUnAvaiIds';

            pendingUnAvais = Database.query(avaiQueryBuilder.getQuery());
        }

        return pendingUnAvais;
    }

    //TODO: get the email template from custom setting, input dynamic data into the template
    //Return the list of emails
    private static List<Messaging.singleEmailmessage> generateUnAvaiEmailBody(List<sked__Availability__c> pendingUnAvais) {
        List<Messaging.singleEmailmessage> emails = new List<Messaging.singleEmailmessage>();
        String emailTemplate = skedConfigs.UNAVAI_NOTIFICATION_EMAIL_TEMPLATE;
        String avaiRequestLink = skedConfigs.AVAILABILITY_REQUEST_LINK;
        String emailSubjectTemplate = skedConfigs.UNAVAI_NOTIFICATION_EMAIL_SUBJECT;

        if (String.isBlank(emailSubjectTemplate)) {
            throw new skedException('Missing Unavailability_Email_Subject in Skedulo Configs Custom Settings');
        }

        if (String.isBlank(emailTemplate)) {
            throw new skedException('Missing Unavailability_Email_Template in Skedulo Configs Custom Settings');
        }

        if (String.isBlank(avaiRequestLink)) {
            throw new skedException('Missing Availability_Request_Link in Skedulo Configs Custom Settings');
        }

        for (sked__Availability__c unAvai : pendingUnAvais) {
            String emailBody = emailTemplate;
            String emailSubject = emailSubjectTemplate;
            DateTime startOfDate = skedDateTimeUtils.getStartOfDate(unAvai.sked__Start__c, unAVai.sked__Timezone__c);
            DateTime endOfDate = skedDateTimeUtils.getEndOfDate(unAvai.sked__Finish__c, unAvai.sked__Timezone__c);
            endOfDate = skedDateTimeUtils.addMinutes(endOfDate, -5, unAvai.sked__Timezone__c);
            boolean isAllDay = false;

            if (unAvai.sked__Start__c == startOfDate && unAvai.sked__Finish__c >= endOfDate) {
                isAllDay = true;
            }

            emailSubject = emailSubject.replace('[RESOURCENAME]', unAvai.sked__Resource__r.name);
            emailBody = emailBody.replace('[RESOURCENAME]', unAvai.sked__Resource__r.name);
            emailBody = emailBody.replace('[AVAI_TYPE]', unAvai.sked__Type__c);

            String startDate = unAvai.sked__Start__c.format(skedDateTimeUtils.DATE_FORMAT, unAvai.sked__Timezone__c);
            String startTime = unAvai.sked__Start__c.format(skedDateTimeUtils.TIME_FORMAT, unAvai.sked__Timezone__c);
            emailBody = emailBody.replace('[STARTDATE]', startDate);
            if (isAllDay) {
                emailBody = emailBody.replace('[STARTTIME]', 'All Day');
            }
            else {
                emailBody = emailBody.replace('[STARTTIME]', startTime);
            }
            String endDate = unAVai.sked__Finish__c.format(skedDateTimeUtils.DATE_FORMAT, unAvai.sked__Timezone__c);
            String endTime = unAVai.sked__Finish__c.format(skedDateTimeUtils.TIME_FORMAT, unAvai.sked__Timezone__c);
            emailBody = emailBody.replace('[ENDDATE]', endDate);
            if (isAllDay) {
                emailBody = emailBody.replace('[ENDTIME]', 'All Day');
            }
            else {
                emailBody = emailBody.replace('[ENDTIME]', endTime);
            }

            emailBody = emailBody.replace('[WEBAPP_LINK]', avaiRequestLink);

            Messaging.singleEmailmessage email = new Messaging.singleEmailmessage();
            email.setsubject(emailSubject);
            email.SetPlainTextbody(emailBody);
            emails.add(email);
        }

        return emails;
    }

    //TODO: get list of recepient email from chatter group
    public static List<String> getrecipientEmailAddress() {
        Set<String> emailAddresses = new Set<String>();
        String chatterGroupName = skedConfigs.AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME;

        if (String.isBlank(chatterGroupName)) {
            throw new skedException('Missing Availability_Notify_Chatter_Group_Name in Skedulo Configs Custom Settings');
        }

        List<CollaborationGroupMember> groupMembers = [SELECT id, Member.Name, Member.email
                                                        FROM CollaborationGroupMember
                                                        WHERE CollaborationGroupId IN (SELECT id FROM CollaborationGroup WHERE Name = :chatterGroupName)];

        for (CollaborationGroupMember groupMember : groupMembers) {
            if (String.isNotBlank(groupMember.Member.email)) {
                emailAddresses.add(groupMember.Member.email);
            }
        }

        if (test.isRunningTest()) { emailAddresses.add('test@abc.com'); }

        return new List<String>(emailAddresses);
    }

    //update recipient email address to list of emails
    private static void updateEmailRecipientAddresses(List<String> emailAddresses,
                                                        List<Messaging.singleEmailmessage> emails) {
        for (Messaging.singleEmailmessage email : emails) {
            email.SetToAddresses(emailAddresses);
        }
    }

    //TODO: send email and throw error when there is any error happens
    private static void sendEmail(List<Messaging.singleEmailmessage> emails) {
        List<Messaging.sendemailResult> sendingResults = Messaging.sendemail(emails);
        String errorMessage = '';

        for (Messaging.sendemailResult result : sendingResults) {
            if (!result.isSuccess()) {
                List<Messaging.SendEmailError> errors = result.getErrors();
                for (Messaging.SendEmailError error : errors) {
                    errorMessage += error.getMessage() + '<br/>';
                }
            }
        }

        if (String.isNotBlank(errorMessage)) {
            throw new skedException(errorMessage);
        }
    }
}