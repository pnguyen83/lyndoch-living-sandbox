@isTest
public class skedAvailabilityHandler_Test {
	@testSetUp static void createData() {
        sked__Region__c regionQld = new sked__Region__c(
            Name = 'QLD',
            sked__Timezone__c = 'Australia/Queensland'
        );
        insert regionQld;

        sked__Resource__c resource = new sked__Resource__c(
                Name = 'Team Leader',
                sked__Primary_Region__c = regionQld.Id,
                sked__Category__c = 'Team Leader',
                sked__Resource_Type__c = 'Person',
                sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
                sked__GeoLocation__Latitude__s = -27.457730,
                sked__GeoLocation__Longitude__s = 153.037080,
                sked__User__c = UserInfo.getUserId(),
                sked__Is_Active__c = TRUE
        );
        insert resource;

        //create custom settings
        skedRACConfigs__c emailSubject = new skedRACConfigs__c(
            Name = 'Unavailability_Email_Subject',
            Value__c = 'New Unavailability Request: [RESOURCENAME]'
        );

        skedRACConfigs__c emailTemplate = new skedRACConfigs__c(
            Name = 'Unavailability_Email_Template',
            Value__c = '[RESOURCENAME] has submitted a new unavailability request that requires approval'
        );

        skedRACConfigs__c requestLink = new skedRACConfigs__c(
            Name = 'Availability_Request_Link',
            Value__c = 'request link'
        );

        skedRACConfigs__c chatterGroupName = new skedRACConfigs__c(
            Name = 'Availability_Notify_Chatter_Group_Name',
            Value__c = 'At Home Office Team'
        );

        insert new List<sObject>{emailSubject, emailTemplate, requestLink, chatterGroupName};
    }

    testmethod static void testInsertUnavaiWithoutEmailSubject() {
        skedRACConfigs__c emailSubject = [SELECT id from skedRACConfigs__c WHERE Name = 'Unavailability_Email_Subject' LIMIT 1];
        delete emailSubject;
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = System.now(),
                sked__Finish__c = System.now().addhours(2),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            System.debug('message ' + ex.getMessage());
            System.assert(ex.getMessage().contains('Missing Unavailability_Email_Subject in Skedulo Configs Custom Settings'));
        }
        test.stopTest();
    }

    testmethod static void testInsertUnavaiWithoutEmailTemplate() {
        skedRACConfigs__c emailTemplate = [SELECT id from skedRACConfigs__c WHERE Name = 'Unavailability_Email_Template' LIMIT 1];
        delete emailTemplate;
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = System.now(),
                sked__Finish__c = System.now().addhours(2),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            System.assert(ex.getMessage().contains('Missing Unavailability_Email_Template in Skedulo Configs Custom Settings'));
        }
        test.stopTest();
    }

    testmethod static void testInsertUnavaiWithoutRequestLink() {
        skedRACConfigs__c emailTemplate = [SELECT id from skedRACConfigs__c WHERE Name = 'Availability_Request_Link' LIMIT 1];
        delete emailTemplate;
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = System.now(),
                sked__Finish__c = System.now().addhours(2),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            System.assert(ex.getMessage().contains('Missing Availability_Request_Link in Skedulo Configs Custom Settings'));
        }
        test.stopTest();
    }

    testmethod static void testInsertUnavaiWithoutGroupName() {
        skedRACConfigs__c emailTemplate = [SELECT id from skedRACConfigs__c WHERE Name = 'Availability_Notify_Chatter_Group_Name' LIMIT 1];
        delete emailTemplate;
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = System.now(),
                sked__Finish__c = System.now().addhours(2),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            System.assert(ex.getMessage().contains('Missing Availability_Notify_Chatter_Group_Name in Skedulo Configs Custom Settings'));
        }
        test.stopTest();
    }

    testmethod static void testInsertUnavai() {
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        String error = '';
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = System.now(),
                sked__Finish__c = System.now().addhours(2),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            error = ex.getMessage();
        }
        System.debug('error ' + error);
        System.assert(String.isBlank(error));
        test.stopTest();
    }

    testmethod static void testInsertFullDayUnavai() {
        sked__Resource__c res = [SELECT id FROM sked__Resource__c LIMIT 1];
        test.startTest();
        String error = '';
        try {
            sked__Availability__c unAvai = new sked__Availability__c(
                sked__Resource__c = res.id,
                sked__Start__c = skedDateTimeUtils.getStartOfDate(System.now(), UserInfo.getTimeZone().getId()),
                sked__Finish__c = skedDateTimeUtils.getEndOfDate(System.now(), UserInfo.getTimeZone().getId()),
                sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING,
                sked__Type__c = 'Annual Leave',
                sked__Is_Available__c = false
            );

            insert unAvai;
        }
        catch (Exception ex) {
            error = ex.getMessage();
        }
        System.debug('error ' + error);
        System.assert(String.isBlank(error));
        test.stopTest();
    }
}