global virtual class skedConfigs {

    private static Map<String, skedRACConfigs__c> skedConfigsInstance = null;

    private static final String CLIENT_JOB_CONFLICT = 'Job Conflict';
    private static final String CLIENT_AVAILABILITY = 'Availability';
    /**
    *@description Singleton to get skedConfigs custom setting
    *
    */
    global static Map<String, skedRACConfigs__c> getskedConfigsCustomSettings() {
        if(skedConfigsInstance == null) {
            skedConfigsInstance = skedRACConfigs__c.getAll();
        }
        return skedConfigsInstance;
    }

    global static String GOOGLE_API_KEY{
        get{
            if(GOOGLE_API_KEY == null){
                if(getskedConfigsCustomSettings().containsKey('Global_GoogleAPIKEY')){
                    GOOGLE_API_KEY     = getskedConfigsCustomSettings().get('Global_GoogleAPIKEY').Value__c;
                }else GOOGLE_API_KEY   = '';
            }
            return GOOGLE_API_KEY;
        }
    }

    global static String UNAVAI_NOTIFICATION_EMAIL_SUBJECT{
        get{
            if(UNAVAI_NOTIFICATION_EMAIL_SUBJECT == null){
                if(getskedConfigsCustomSettings().containsKey('Unavailability_Email_Subject')){
                    UNAVAI_NOTIFICATION_EMAIL_SUBJECT     = getskedConfigsCustomSettings().get('Unavailability_Email_Subject').Value__c;
                }else UNAVAI_NOTIFICATION_EMAIL_SUBJECT   = '';
            }
            return UNAVAI_NOTIFICATION_EMAIL_SUBJECT;
        }
    }

    global static String UNAVAI_NOTIFICATION_EMAIL_TEMPLATE{
        get{
            if(UNAVAI_NOTIFICATION_EMAIL_TEMPLATE == null){
                if(getskedConfigsCustomSettings().containsKey('Unavailability_Email_Template')){
                    UNAVAI_NOTIFICATION_EMAIL_TEMPLATE     = getskedConfigsCustomSettings().get('Unavailability_Email_Template').Value__c;
                }else UNAVAI_NOTIFICATION_EMAIL_TEMPLATE   = '';
            }
            return UNAVAI_NOTIFICATION_EMAIL_TEMPLATE;
        }
    }

    global static String AVAILABILITY_REQUEST_LINK{
        get{
            if(AVAILABILITY_REQUEST_LINK == null){
                if(getskedConfigsCustomSettings().containsKey('Availability_Request_Link')){
                    AVAILABILITY_REQUEST_LINK     = getskedConfigsCustomSettings().get('Availability_Request_Link').Value__c;
                }else AVAILABILITY_REQUEST_LINK   = '';
            }
            return AVAILABILITY_REQUEST_LINK;
        }
    }

    global static String AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME{
        get{
            if(AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME == null){
                if(getskedConfigsCustomSettings().containsKey('Availability_Notify_Chatter_Group_Name')){
                    AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME     = getskedConfigsCustomSettings().get('Availability_Notify_Chatter_Group_Name').Value__c;
                }else AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME   = '';
            }
            return AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME;
        }
    }

    global static String RESOURCE_DISTANCE_FIRST    = 'First';
    global static String RESOURCE_DISTANCE_COMBINED    = 'Combined';
    global static String DISTANCE_TRAVELLED_SPLIT2    = 'Split';
    global static String DISTANCE_TRAVELLED_ALL    = 'All';


}